# Wiktoria Rebacz

##Steps to start the application
### `yarn install`

Installs all needed libs.

### `yarn start`
Starts the application. It should automatically open the app in a browser at `http://localhost:3000/`.
If it does not but you can see `Compiled successfully` in the console, please go to `http://localhost:3000/` 
manually. 

**Browser**
Application works best with Chrome, but was also tested with Safari and Firefox.
