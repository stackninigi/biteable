export const states = {
  NEW_SOUTH_WALES: "nsw",
  TASMANIA: "tas",
  WESTERN_AUSTRALIA: "wa",
  VICTORIA: "vic",
  SOUTH_AUSTRALIA: "sa",
  QUEENSLAND: "qld"
};

export const territories = {
  THE_NORTHEN_TERRITORY: "nt",
  THE_AUSTRALIAN_CAPITAL_TERRITORY: "act"
};
