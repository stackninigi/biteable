export const funFacts = {
  ABORIGINES:
    "Australia was inhabited for at lest 50 000 years before the British came. The Aboriginal people have lived in Australia for at least 50 000 years. They’re generally considered to have the oldest living culture on Earth.",
  DEADLY_ANIMALS:
    "They are lucky to still be alive. Australia is home to the deadliest spider, the deadliest snake, the deadliest octopus, the deadliest fish and so on. But despite this, very few people are actually killed by these highly venomous animals. From the year 2000 and 2013, more people were even killed by horses than Australia’s deadliest animals combined.",
  CROWDED:
    "Australia is the 6th largest country by size, a massive 2.9 million square miles. Australia is very sparsely populated: The UK has 248.25 persons per square kilometre, while Australia has only 2.66 persons per square kilometre.Some data reports that the total population of Kangaroos is almost 50 million now. Back in 2016, there were almost 45 million Kangaroos, which means that there are more Kangaroos in Australia than people!",
  SWAN_RIVER:
    'The Swan River Colony was a British colony established in 1829 on the Swan River, in Western Australia. The name was a pars pro toto for Western Australia. In 1832 the colony was renamed the Colony of Western Australia, when the colony\'s founding lieutenant-governor, Captain James Stirling, belatedly received his commission. However, the name "Swan River Colony" remained in informal use for many years afterwards.',
  MELBOURNE:
    "Melbourne, the biggest city in Victoria state, is considered the sporting capital of the world, as it has more top level sport available for its citizens than anywhere else",
  TASMANIA:
    "The island of Tasmania is the ideal spot to experience the great outdoors - with air as clean as Antarctica, around one third of the state is a national park or World Heritage protected. It is a walkers paradise, with trails and walkways winding over the whole island. ",
  STATISTICS:
    "Despite being a massive continent, 90% of Australia's population live on the coast due to the majority of the interior being a vast desert. Australia's Aboriginal people are estimated to have lived here for roughly 50,000 years, yet they now make up only 1.5% of the total population.",
  TOMMY: "Basically a chilled out entertainer. Tommy Fotak, CTO & Co-Founder.",
  STATES_AND_TERRITORIES:
    'The Australian mainland consists of five of the six federated states and three of the federal territories (the "internal" territories). '
};
