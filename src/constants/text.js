export const WideCardText = {
  QUESTION:
    "Interested to find out what is a State’s population and its percent of the National population?",
  DROPDOWN_YEAR: "Year",
  DROPDOWN_NAME: "Name"
};

export const PopulationUtilsText = {
  START: "Start",
  END: "End",
  ONLY_ONE_YEAR: "Only one year"
};

export const CardListText = {
  TITLE: "So no one told you Australia was gonna be this way?"
};
