import { states, territories } from "../constants/statesAndTerritories";
import * as fromUtils from "../utils/populationUtils";
import { funFacts } from "./funFacts";
import { mapObjectToArray } from "../utils/jsonUtils";

import Decrease from "../assets/Decrease.png";
import Population1788 from "../assets/Population1788.svg";
import Melbourne from "../assets/Melbourne.svg";
import Population2011 from "../assets/Population2011.jpg";
import Kangaroo from "../assets/Kangaroo.jpg";
import Crowd from "../assets/Crowd.svg";
import River from "../assets/River.svg";
import Tommy from "../assets/Tommy.svg";
import TasmanianDevil from "../assets/TasmiananDevil.jpg";
import Territory from "../assets/Territory.jpg";

export const questions = [
  {
    question: "What is the total population of Australia in 1788?",
    method: fromUtils.getPopulationCount,
    args: [1788],
    showCounting: true,
    img: Population1788,
    funFact: funFacts.ABORIGINES,
    title: "The one with the beginning"
  },
  {
    question: "What is the total population of Australia in 2011?",
    method: fromUtils.getPopulationCount,
    args: [2011],
    showCounting: true,
    img: Population2011,
    funFact: funFacts.DEADLY_ANIMALS,
    title: "The one set in 2011"
  },
  {
    question: "Between what years was Victoria the most populous State?",
    method: fromUtils.getYearsWhenLandMostPopulous,
    args: [states.VICTORIA],
    img: Melbourne,
    funFact: funFacts.MELBOURNE,
    title: "The one with victory"
  },
  {
    question: "Visualise the combined population of the States over time.",
    method: fromUtils.getPopulationCount,
    args: [1788, Array.from(mapObjectToArray(states)), 2011],
    showChart: true,
    img: Kangaroo,
    funFact: funFacts.STATES_AND_TERRITORIES,
    title: "The one with combined population"
  },
  {
    question:
      "Rank the States and Territories in descending order by population in 1999.",
    method: fromUtils.getLandsInDescendingOrder,
    args: [1999],
    img: Decrease,
    title: "The one starting big ending small"
  },
  {
    question: "Visualise Tasmania’s population rank over time.",
    method: fromUtils.getPopulationCount,
    args: [1788, states.TASMANIA, 2011],
    showChart: true,
    img: TasmanianDevil,
    funFact: funFacts.TASMANIA,
    title: "The one in Tasmania"
  },
  {
    question: "What year did Australia surpass 20m people?",
    method: fromUtils.findWhenCountWasSurpassed,
    args: [20000000],
    img: Crowd,
    funFact: funFacts.CROWDED,
    title: "The one when it gets crowded"
  },
  {
    question: "Visualise the combined population of the Territories over time.",
    method: fromUtils.getPopulationCount,
    args: [1788, Array.from(mapObjectToArray(territories)), 2011],
    showChart: true,
    img: Territory,
    funFact: funFacts.STATES_AND_TERRITORIES,
    title: "The one on foreign territory"
  },
  {
    question:
      "Tommy our CTO was born in 1979, what was the increase in population in Tasmania in the year he was born?",
    method: fromUtils.getPopulationIncrease,
    args: [states.TASMANIA, 1979],
    showCounting: true,
    title: "The one with Tommy, the CTO",
    funFact: funFacts.TOMMY,
    img: Tommy
  },
  {
    question:
      "In what year was the Western Australian colony of Swan River established?",
    method: fromUtils.getEstablishmentYear,
    args: [states.WESTERN_AUSTRALIA],
    img: River,
    funFact: funFacts.SWAN_RIVER,
    title: "The one with swans in a river"
  }
];
