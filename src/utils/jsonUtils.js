import { states, territories } from "../constants/statesAndTerritories";

export const mapObject = (object, mapFunction) =>
  Object.keys(object).reduce((result, key) => {
    result[key] = mapFunction(object[key]);
    return result;
  }, {});

export const mapStringToNumber = json =>
  json.map(obj =>
    mapObject(obj, number => (!!number ? Number.parseInt(number) : 0))
  );

export function* mapObjectToArray(obj) {
  for (let prop of Object.keys(obj)) yield obj[prop];
}

export function* createArray(start, end, step) {
  while (start < end) {
    yield start;
    start += step;
  }
}

export const getLandName = value => {
  const lands = { ...states, ...territories };
  const key = Object.keys(lands).find(key => lands[key] === value);
  return key
    .replace(/_/g, " ")
    .toLowerCase()
    .split(" ")
    .map(s => s.charAt(0).toUpperCase() + s.substring(1))
    .join(" ");
};
