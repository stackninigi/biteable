import {
  mapObject,
  mapStringToNumber,
  mapObjectToArray,
  createArray,
  getLandName
} from "../jsonUtils";

describe("jsonUtils", () => {
  describe("mapObject", () => {
    const object = {
      a: 1,
      b: 2
    };

    it("should allow to map objects", () => {
      expect(mapObject(object, val => val * 2)).toEqual({ a: 2, b: 4 });
    });
  });

  describe("mapStringToNumber", () => {
    it("should convert string to number", () => {
      const result = mapStringToNumber([{ test: "1" }]);
      expect(result).toEqual([{ test: 1 }]);
    });
  });

  describe("mapObjectToArray", () => {
    const object = {
      a: 1,
      b: 2
    };
    it("should convert object to array", () => {
      const result = Array.from(mapObjectToArray(object));
      expect(result).toEqual([1, 2]);
    });
  });

  describe("createArray", () => {
    it("should create array", () => {
      const result = Array.from(createArray(11, 15, 2));
      expect(result).toEqual([11, 13]);
    });
  });

  describe("getLandName", () => {
    it("should get state name based on key", () => {
      expect(getLandName("nsw")).toEqual("New South Wales");
    });
  });
});
