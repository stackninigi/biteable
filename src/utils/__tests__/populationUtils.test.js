import {
  getPopulationCount,
  getPopulationAndNationalPercentage,
  getEstablishmentYear,
  getPopulationIncrease,
  findWhenCountWasSurpassed,
  findMostPopulousYears,
  rankLandsInDescendingOrder
} from "../populationUtils";
import { states, territories } from "../../constants/statesAndTerritories";
import { mapObjectToArray } from "../jsonUtils";

describe("populationUtils", () => {
  const data = [
    {
      act: 0,
      nsw: 1,
      nt: 1,
      qld: 0,
      sa: 0,
      tas: 0,
      vic: 2,
      wa: 0,
      year: 1788
    },
    {
      act: 1,
      nsw: 1,
      nt: 1,
      qld: 1,
      sa: 1,
      tas: 1,
      vic: 1,
      wa: 1,
      year: 1789
    }
  ];

  describe("getPopulationCount", () => {
    it("should return whole population count of given year", () => {
      expect(getPopulationCount(data, 1789)).toEqual(8);
    });

    it("should return Tasmania population count of given year", () => {
      expect(
        getPopulationCount(
          data,
          1788,
          Array.from(mapObjectToArray({ ...states, ...territories })),
          1789
        )
      ).toEqual([{ x: 1788, y: 4 }, { x: 1789, y: 8 }]);
    });

    it("", () => {});
  });

  describe("getPopulationAndNationalPercentage", () => {
    it("should return population count and national percentage", () => {
      expect(
        getPopulationAndNationalPercentage(data, 1788, states.VICTORIA)
      ).toEqual({
        populationCount: 2,
        nationalPercentage: 50
      });
    });
  });

  describe("getEstablishmentYear", () => {
    it("should return establishment year of chosen state", () => {
      const data = [
        { a: 0, b: 0, year: 2001 },
        { a: 0, b: 1, year: 2002 },
        { a: 1, b: 1, year: 2003 },
        { a: 0, b: 1, year: 2004 },
        { a: 1, b: 1, year: 2005 },
        { a: 1, b: 1, year: 2006 }
      ];
      expect(getEstablishmentYear(data, "a")).toEqual(2003);
    });
  });

  describe("getPopulationIncrease", () => {
    it("should return increase in chosen state in chosen year", () => {
      const data = [
        { a: 13, b: 0, year: 2001 },
        { a: 16, b: 1, year: 2002 },
        { a: 14, b: 1, year: 2003 },
        { a: 19, b: 1, year: 2004 },
        { a: 21, b: 1, year: 2005 },
        { a: 25, b: 1, year: 2006 }
      ];
      expect(getPopulationIncrease(data, "a", 2005)).toEqual(2);
    });
  });

  describe("findWhenCountWasSurpassed", () => {
    it("should return year when a number was surpassed", () => {
      expect(findWhenCountWasSurpassed(data, 4)).toEqual(1789);
    });
  });

  describe("findMostPopulousYears", () => {
    it("should return first and last year when Tasmania was the most populous state", () => {
      const data = [
        {
          act: 0,
          nsw: 1,
          nt: 1,
          qld: 0,
          sa: 0,
          tas: 0,
          vic: 2,
          wa: 0,
          year: 1788
        },
        {
          act: 0,
          nsw: 1,
          nt: 1,
          qld: 0,
          sa: 0,
          tas: 3,
          vic: 2,
          wa: 0,
          year: 1789
        },
        {
          act: 0,
          nsw: 1,
          nt: 1,
          qld: 0,
          sa: 0,
          tas: 4,
          vic: 2,
          wa: 0,
          year: 1790
        },
        {
          act: 0,
          nsw: 1,
          nt: 1,
          qld: 0,
          sa: 0,
          tas: 1,
          vic: 2,
          wa: 0,
          year: 1791
        },
        {
          act: 0,
          nsw: 1,
          nt: 1,
          qld: 0,
          sa: 0,
          tas: 3,
          vic: 2,
          wa: 0,
          year: 1792
        },
        {
          act: 0,
          nsw: 1,
          nt: 1,
          qld: 0,
          sa: 0,
          tas: 3,
          vic: 2,
          wa: 0,
          year: 1793
        }
      ];
      expect(findMostPopulousYears(data, "tas")).toEqual([
        { end: 1790, start: 1789 },
        { end: 1793, start: 1792 }
      ]);
    });
  });

  describe("rankLandsInDescendingOrder", () => {
    it("should return lands in descending order", () => {
      const data = {
        act: 0,
        nsw: 1,
        nt: 1,
        qld: 0,
        sa: 0,
        tas: 3,
        vic: 2,
        wa: 0
      };
      expect(rankLandsInDescendingOrder(data)).toEqual([
        "tas",
        "vic",
        "nsw",
        "nt",
        "act",
        "qld",
        "sa",
        "wa"
      ]);
    });
  });
});
