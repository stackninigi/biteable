import React from "react";

import { states, territories } from "../constants/statesAndTerritories";
import { PopulationUtilsText } from "../constants/text";
import { getLandName } from "./jsonUtils";

const getChosenLandsYearlyPopulation = (data, i, names) => {
  return names.reduce(
    (previousValue, currentValue) =>
      previousValue + getSingleYearPopulationCount(data, i, currentValue),
    0
  );
};

const getCountPerYear = (data, i, names) =>
  !names
    ? getSingleYearPopulationCount(data, i)
    : Array.isArray(names)
    ? getChosenLandsYearlyPopulation(data, i, names)
    : getSingleYearPopulationCount(data, i, names);

const getRangePopulationCount = (data, firstYear, lastYear, names) =>
  [...Array(lastYear - firstYear + 1).keys()].map(i => ({
    x: firstYear + i,
    y: getCountPerYear(data, firstYear + i, names)
  }));

const getYearlyPopulation = (names, value) =>
  Object.values(names).reduce(
    (previousValue, currentValue) => value[currentValue] + previousValue,
    0
  );

const getWholeLandYearCount = value =>
  getYearlyPopulation(states, value) + getYearlyPopulation(territories, value);

const getSingleYearPopulationCount = (data, year, names) =>
  data.reduce((previousValue, currentValue) => {
    const value =
      !!names && currentValue.year === year
        ? currentValue[names]
        : getWholeLandYearCount(currentValue, year);
    return currentValue.year === year ? previousValue + value : previousValue;
  }, 0);

export const getPopulationCount = (data, year, names, lastYear) =>
  lastYear
    ? getRangePopulationCount(data, year, lastYear, names)
    : getSingleYearPopulationCount(data, year, names);

export const getPopulationAndNationalPercentage = (data, year, name) => {
  const nationalCount = getPopulationCount(data, year);
  const stateCount = getPopulationCount(data, year, name);
  return {
    populationCount: stateCount,
    nationalPercentage: (stateCount / nationalCount) * 100
  };
};

export const getEstablishmentYear = (data, name) =>
  data.find(value => value[name] > 0).year;

export const getPopulationIncrease = (data, name, year) => {
  const previousYear = getPopulationCount(data, year - 1, name);
  const givenYear = getPopulationCount(data, year, name);
  const result = givenYear - previousYear;
  return result > 0 ? result : 0;
};

export const findMostPopulatedState = population =>
  Object.keys(population).reduce((max, x) =>
    population[x] > population[max] ? x : max
  );

const isFirstYearFirstInterval = lastInterval => !lastInterval[0];

const isFirstYearAnotherInterval = allPrevious => allPrevious.length === 0;

const isEndOfInterval = lastInterval =>
  !lastInterval[0].end && lastInterval[0].start;

const isBiggerThanDirectPrev = (lastInterval, year) =>
  lastInterval[0].end === year - 1;

export const findMostPopulousYears = (data, name) =>
  data.reduce((prev, { year, ...dataWithoutYear }) => {
    const mostPopulatedLand = findMostPopulatedState(dataWithoutYear);
    if (mostPopulatedLand === name) {
      const allPrevious = prev.slice(0, -1);
      const lastInterval = prev.slice(-1);
      if (isFirstYearFirstInterval(lastInterval)) {
        return [...allPrevious, { start: year }];
      }
      if (
        isEndOfInterval(lastInterval) ||
        isBiggerThanDirectPrev(lastInterval, year)
      ) {
        return [...allPrevious, { start: lastInterval[0].start, end: year }];
      }
      if (isFirstYearAnotherInterval(allPrevious)) {
        return [...lastInterval, { start: year }];
      }
      return [...allPrevious, ...lastInterval];
    }
    return prev;
  }, []);

const renderOnlyOneYear = start => (
  <React.Fragment>
    {PopulationUtilsText.ONLY_ONE_YEAR}:{start}
  </React.Fragment>
);

const renderFirstLastYear = (first, last) => (
  <React.Fragment>
    {PopulationUtilsText.START}:{first} {PopulationUtilsText.END}: {last}
  </React.Fragment>
);

export const getYearsWhenLandMostPopulous = (data, name) =>
  findMostPopulousYears(data, name).map(({ start, end }) => (
    <React.Fragment>
      {start && !end
        ? renderOnlyOneYear(start)
        : renderFirstLastYear(start, end)}
    </React.Fragment>
  ));

export const findWhenCountWasSurpassed = (data, count) => {
  let sum = 0;
  return data.find(
    ({ year }) => (sum += getPopulationCount(data, year)) > count
  ).year;
};

export const rankLandsInDescendingOrder = dataWithoutYear =>
  Object.keys(dataWithoutYear).sort((a, b) =>
    dataWithoutYear[a] > dataWithoutYear[b]
      ? -1
      : dataWithoutYear[a] < dataWithoutYear[b]
      ? 1
      : 0
  );

export const getLandsInDescendingOrder = (data, rankYear) => {
  const { year, ...dataWithoutYear } = data.find(
    ({ year }) => year === rankYear
  );
  return (
    !!year &&
    rankLandsInDescendingOrder(dataWithoutYear).map((key, index) => (
      <React.Fragment key={`${key}_${index}`}>
        {index + 1}.{getLandName(key)} - {dataWithoutYear[key]}
      </React.Fragment>
    ))
  );
};
