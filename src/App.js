import React, { Component } from "react";

import TestComponent from "./components/CardsList/CardsList";
import ContextProvider from "./components/ContextProvider/ContextProvider";

class App extends Component {
  render() {
    return (
      <ContextProvider>
        <TestComponent />
      </ContextProvider>
    );
  }
}

export default App;
