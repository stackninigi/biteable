import React from "react";
import Enzyme, { mount, shallow } from "enzyme";

import FocusedItem from "../FocusedItem";

let shallowWrapper;
const mockedData = {
  method: jest.fn(),
  args: [],
  question: "test question",
  funFact: "test fun fact",
  img: {},
  showCounting: false
};

describe("FocusedItem", () => {
  beforeEach(() => {
    shallowWrapper = shallow(<FocusedItem {...mockedData} />);
  });

  it("should render without exploding", () => {
    expect(shallowWrapper).toHaveLength(1);
  });

  it("should render matching snapshot", () => {
    expect(shallowWrapper).toMatchSnapshot();
  });
});
