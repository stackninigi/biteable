import React from "react";
import CountUp from "react-countup";
import PropTypes from "prop-types";

import { MyConsumer } from "../ContextProvider/ContextProvider";
import Chart from "../Chart/Chart";

import "./FocusedItem.scss";

const FocusedItem = ({
  method,
  args,
  showCounting,
  question,
  img,
  funFact,
  showChart
}) => {
  const renderResult = result => {
    if (showCounting) {
      return <CountUp end={result} duration={4} />;
    }
    if (Array.isArray(result)) {
      return (
        <React.Fragment>
          {result.map((value, index) => (
            <p key={`${value}__${index}`}>{value}</p>
          ))}
        </React.Fragment>
      );
    }
    return result;
  };

  const renderContent = state => {
    const result = method(state.population, ...args);
    return showChart ? (
      <React.Fragment>
        <div className="FocusedItem__chartWrapper">
          <Chart data={result} />
        </div>
        <p className="FocusedItem__textWrapper">{funFact}</p>
      </React.Fragment>
    ) : (
      <React.Fragment>
        <img
          src={img}
          className="FocusedItem__image"
          alt="FocusedItem__image"
        />
        <div>
          <h3>{renderResult(result)}</h3>
          <p>{funFact}</p>
        </div>
      </React.Fragment>
    );
  };

  return (
    <div className="FocusedItem__wrapper">
      <h2>{question}</h2>
      <div className="FocusedItem__contentWrapper">
        <MyConsumer>{({ state }) => renderContent(state)}</MyConsumer>
      </div>
    </div>
  );
};

FocusedItem.propTypes = {
  method: PropTypes.func,
  args: PropTypes.arrayOf(PropTypes.any),
  question: PropTypes.string,
  funFact: PropTypes.string,
  img: PropTypes.node,
  showCounting: PropTypes.bool,
  showChart: PropTypes.bool
};

export default FocusedItem;
