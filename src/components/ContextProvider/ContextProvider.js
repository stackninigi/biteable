import React, { Component } from "react";
import { mapStringToNumber } from "../../utils/jsonUtils";
import biteable from "../../api/biteable";

const MyContext = React.createContext();

export const MyConsumer = MyContext.Consumer;

class ContextProvider extends Component {
  constructor(props) {
    super(props);
    this.state = { population: [] };
  }

  componentDidMount() {
    // usually here would be an API call
    this.setState({
      population: mapStringToNumber(biteable)
    });
  }

  render() {
    return (
      <MyContext.Provider value={{ state: this.state }}>
        {this.props.children}
      </MyContext.Provider>
    );
  }
}

export default ContextProvider;
