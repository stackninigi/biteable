import React from "react";
import PropTypes from "prop-types";
import { VictoryChart, VictoryLine, VictoryTheme, VictoryAxis } from "victory";

const Chart = ({ data }) => (
  <VictoryChart theme={VictoryTheme.material}>
    <VictoryAxis
      tickValues={[1788, 1850, 1900, 1950, 2011]}
      tickFormat={["1788", "1850", "1900", "1950", "2011"]}
    />
    <VictoryAxis dependentAxis tickFormat={x => `${x / 1000}k`} />
    <VictoryLine
      style={{
        data: { stroke: "#c43a31" },
        parent: { border: "1px solid #ccc" }
      }}
      data={data}
      animate={{
        duration: 2000,
        onLoad: { duration: 1000 }
      }}
    />
  </VictoryChart>
);

Chart.propTypes = {
  data: PropTypes.arrayOf(
    PropTypes.shape({
      x: PropTypes.number,
      y: PropTypes.number
    })
  )
};

export default Chart;
