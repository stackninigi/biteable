import React from "react";
import { shallow } from "enzyme";

import Chart from "../Chart";

let shallowWrapper;
const mockData = {
  data: [{ x: 1, y: 1 }]
};

describe("Chart", () => {
  beforeEach(() => {
    shallowWrapper = shallow(<Chart {...mockData} />);
  });

  it("should render without exploding", () => {
    expect(shallowWrapper).toHaveLength(1);
  });

  it("should render matching snapshot", () => {
    expect(shallowWrapper).toMatchSnapshot();
  });

  it("should render 1 VictoryChart components", () => {
    expect(shallowWrapper.find("VictoryChart")).toHaveLength(1);
  });

  it("should render 1 VictoryLine components", () => {
    expect(shallowWrapper.find("VictoryLine")).toHaveLength(1);
  });

  it("should render 2 VictoryAxis components", () => {
    expect(shallowWrapper.find("VictoryAxis")).toHaveLength(2);
  });
});
