import React from "react";
import CountUp from "react-countup";
import PropTypes from "prop-types";

import DropDown from "../DropDown/DropDown";
import { createArray, mapObjectToArray } from "../../utils/jsonUtils";
import { states } from "../../constants/statesAndTerritories";
import { MyConsumer } from "../ContextProvider/ContextProvider";
import { getPopulationAndNationalPercentage } from "../../utils/populationUtils";
import { WideCardText } from "../../constants/text";
import { funFacts } from "../../constants/funFacts";
import Chart from "../../assets/Chart.jpg";

import "./WideCard.scss";

const WideCard = ({
  onYearChange,
  onNameChange,
  onDropDownSubmit,
  showResults,
  name,
  year
}) => {
  const renderResult = state => {
    const result = getPopulationAndNationalPercentage(
      state.population,
      year,
      name
    );
    return (
      <div className="WideCard__result">
        <img src={Chart} className="WideCard__image" alt="WideCard__image" />
        <div className="WideCard__counts">
          {Object.entries(result).map((values, index) => (
            <React.Fragment key={`${values[0]}__${index}`}>
              <h3>
                {values[0]}: <CountUp end={values[1]} duration={3} />
              </h3>
            </React.Fragment>
          ))}
          <p>{funFacts.STATISTICS}</p>
        </div>
      </div>
    );
  };

  return (
    <div className="WideCard__wrapper">
      <div className="WideCard__contentWrapper">
        <h2>{WideCardText.QUESTION}</h2>
        <div className="WideCard__dropDowns">
          <DropDown
            options={Array.from(createArray(1788, 2012, 1))}
            label={WideCardText.DROPDOWN_YEAR}
            onChange={onYearChange}
          />
          <DropDown
            options={Array.from(mapObjectToArray(states))}
            label={WideCardText.DROPDOWN_NAME}
            onChange={onNameChange}
          />
        </div>
        <button onClick={onDropDownSubmit}>submit</button>
        {showResults && (
          <MyConsumer>
            {({ state }) => (
              <React.Fragment>{renderResult(state)}</React.Fragment>
            )}
          </MyConsumer>
        )}
      </div>
    </div>
  );
};

WideCard.propTYpes = {
  onYearChange: PropTypes.func,
  onNameChange: PropTypes.func,
  onDropDownSubmit: PropTypes.func,
  showResults: PropTypes.bool,
  name: PropTypes.string,
  year: PropTypes.string
};

export default WideCard;
