import React from "react";
import { shallow } from "enzyme";

import WideCard from "../WideCard";

let shallowWrapper;
const mockProps = {
  onYearChange: jest.fn(),
  onDropDownSubmit: jest.fn(),
  showResults: false,
  name: "name",
  year: 123
};

describe("WideCard", () => {
  beforeEach(() => {
    shallowWrapper = shallow(<WideCard {...mockProps} />);
  });

  it("should render without exploding", () => {
    expect(shallowWrapper).toHaveLength(1);
  });

  it("should render matching snapshot", () => {
    expect(shallowWrapper).toMatchSnapshot();
  });

  it("should render 2 DropDown components", () => {
    expect(shallowWrapper.find("DropDown")).toHaveLength(2);
  });

  it("should call onDropDownSubmit on button click", () => {
    shallowWrapper.find("button").simulate("click");
    expect(mockProps.onDropDownSubmit).toHaveBeenCalledTimes(1);
  });
});
