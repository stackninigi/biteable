import React from "react";
import PropTypes from "prop-types";

import "./Item.scss";

const ItemStyle = {
  backgroundPosition: "center",
  backgroundSize: "cover",
  backgroundRepeat: "no-repeat",
  height: "100%",
  position: "absolute",
  left: 0,
  right: 0,
  top: 0,
  bottom: 0
};

const Item = ({ img, title }) => (
  <div className="Item__wrapper">
    <span className="Item__title">
      <h2>{title}</h2>
    </span>
    <div
      style={{ ...ItemStyle, backgroundImage: `url(${img})` }}
      className="Item__image"
    />
  </div>
);

Item.propTypes = {
  img: PropTypes.node,
  title: PropTypes.string
};

export default Item;
