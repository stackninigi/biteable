import React from "react";
import { shallow } from "enzyme";

import Item from "../Item";

let shallowWrapper;
const testTitle = "testTitle";

describe("Item", () => {
  beforeEach(() => {
    shallowWrapper = shallow(<Item title={testTitle} img="" />);
  });

  it("should render without exploding", () => {
    expect(shallowWrapper).toHaveLength(1);
  });

  it("should render matching snapshot", () => {
    expect(shallowWrapper).toMatchSnapshot();
  });

  it("should render one h2 tag", () => {
    expect(shallowWrapper.find("h2")).toHaveLength(1);
  });
});
