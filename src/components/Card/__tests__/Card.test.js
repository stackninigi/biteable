import React from "react";
import { shallow } from "enzyme";

import Card from "../Card";
import "../Card.scss";

let shallowWrapper;
const mockData = {
  onClick: jest.fn(),
  isFocused: false,
  data: {
    question: "testQuestion"
  }
};

describe("Card", () => {
  beforeEach(() => {
    shallowWrapper = shallow(<Card {...mockData} />);
  });

  it("should render without exploding", () => {
    expect(shallowWrapper).toHaveLength(1);
  });

  it("should render matching snapshot", () => {
    expect(shallowWrapper).toMatchSnapshot();
  });

  it("fire onClick when div clicked", () => {
    shallowWrapper.find("div").simulate("click");
    expect(mockData.onClick).toHaveBeenCalled();
  });

  it("if not focused should have Card__item class name", () => {
    expect(shallowWrapper.find("div").hasClass("Card__item")).toBe(true);
  });
});
