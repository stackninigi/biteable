import React from "react";
import PropTypes from "prop-types";
import { Flipped } from "react-flip-toolkit";

import "./Card.scss";
import FocusedItem from "../FocusedItem/FocusedItem";
import Item from "../Item/Item";

const Card = ({ data, onClick, isFocused }) => {
  const handleClick = () => {
    onClick(data);
  };

  const {
    question,
    method,
    args,
    img,
    funFact,
    title,
    showCounting,
    showChart
  } = data;
  return (
    <Flipped flipId={question}>
      <div
        onClick={handleClick}
        className={isFocused ? "Card__focusedItem" : "Card__item"}
      >
        <Flipped inverseFlipId={question}>
          <Flipped flipId={`${question}-text`} translate>
            {isFocused ? (
              <FocusedItem
                method={method}
                args={args}
                question={question}
                img={img}
                funFact={funFact}
                showCounting={showCounting}
                showChart={showChart}
              />
            ) : (
              <Item img={img} title={title} />
            )}
          </Flipped>
        </Flipped>
      </div>
    </Flipped>
  );
};

Card.propTypes = {
  data: PropTypes.objectOf(
    PropTypes.shape({
      question: PropTypes.string,
      method: PropTypes.func,
      args: PropTypes.array,
      img: PropTypes.node,
      funFact: PropTypes.string,
      title: PropTypes.string,
      showCounting: PropTypes.bool,
      showChart: PropTypes.bool
    })
  ),
  onClick: PropTypes.func,
  isFocused: PropTypes.bool
};

export default Card;
