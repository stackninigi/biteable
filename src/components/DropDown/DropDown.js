import React from "react";
import PropTypes from "prop-types";

import "./DropDown.scss";

const DropDown = ({ onChange, options, label, value }) => {
  const handleChange = event => onChange(event);

  const mapOptions = () =>
    options.map(option => (
      <option value={option} key={option}>
        {option}
      </option>
    ));

  return (
    <form className="DropDown__wrapper">
      <label className="DropDown__label">
        {label}
        <select
          value={value}
          onChange={handleChange}
          className="DropDown__select"
        >
          {mapOptions()}
        </select>
      </label>
    </form>
  );
};

DropDown.propTypes = {
  onChange: PropTypes.func,
  label: PropTypes.string,
  value: PropTypes.string,
  options: PropTypes.array
};

export default DropDown;
