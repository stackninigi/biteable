import React from "react";
import { shallow } from "enzyme";

import DropDown from "../DropDown";

let shallowWrapper;
const mockProps = {
  onChange: jest.fn(),
  label: "label",
  options: ["1", "2"]
};

describe("DropDown", () => {
  beforeEach(() => {
    shallowWrapper = shallow(<DropDown {...mockProps} />);
  });

  it("should render without exploding", () => {
    expect(shallowWrapper).toHaveLength(1);
  });

  it("should render matching snapshot", () => {
    expect(shallowWrapper).toMatchSnapshot();
  });

  it("should render 2 option components", () => {
    expect(shallowWrapper.find("option")).toHaveLength(2);
  });
});
