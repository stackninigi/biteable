import React from "react";
import { shallow } from "enzyme";

import CardsList from "../CardsList";

let shallowWrapper;

describe("CardsList", () => {
  beforeEach(() => {
    shallowWrapper = shallow(<CardsList />);
  });

  it("should render without exploding", () => {
    expect(shallowWrapper).toHaveLength(1);
  });

  it("should render matching snapshot", () => {
    expect(shallowWrapper).toMatchSnapshot();
  });

  it("should render 10 Card components", () => {
    expect(shallowWrapper.find("Card")).toHaveLength(10);
  });

  it("should render 1 WideCard components", () => {
    expect(shallowWrapper.find("WideCard")).toHaveLength(1);
  });
});
