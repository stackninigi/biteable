import React, { Component } from "react";
import { Flipper } from "react-flip-toolkit";

import Card from "../Card/Card";
import WideCard from "../WideCard/WideCard";
import { questions } from "../../constants/questions";
import { states } from "../../constants/statesAndTerritories";
import { CardListText } from "../../constants/text";

import "./CardsList.scss";

class CardsList extends Component {
  state = {
    focused: {
      question: undefined
    },
    name: Object.values(states)[0],
    year: 1788,
    showResults: false
  };

  handleYearChange = event => {
    this.setState({
      year: Number.parseInt(event.target.value),
      showResults: false
    });
  };

  handleNameChange = event =>
    this.setState({ name: event.target.value, showResults: false });

  handleDropDownSubmit = () => {
    this.setState({ showResults: true });
  };

  handleOnOpenClick = question => {
    this.setState({ focused: question });
  };

  handleOnCloseClick = () => {
    this.setState({
      focused: {
        question: null
      }
    });
  };

  renderCard = () => {
    const { focused } = this.state;
    return !!focused.question ? (
      <Card data={focused} onClick={this.handleOnCloseClick} isFocused />
    ) : (
      <div className="CardList__cards">
        {questions.map((question, index) => (
          <Card
            data={question}
            onClick={this.handleOnOpenClick}
            key={index}
            index={`${index}`}
          />
        ))}
      </div>
    );
  };

  render() {
    const { focused, name, year } = this.state;
    return (
      <React.Fragment>
        <h1>{CardListText.TITLE}</h1>
        <Flipper flipKey={focused}>{this.renderCard()}</Flipper>
        <WideCard
          onYearChange={this.handleYearChange}
          onNameChange={this.handleNameChange}
          onDropDownSubmit={this.handleDropDownSubmit}
          showResults={this.state.showResults}
          name={name}
          year={year}
        />
      </React.Fragment>
    );
  }
}

CardsList.propTypes = {};

export default CardsList;
